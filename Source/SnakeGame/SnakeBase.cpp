// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"
#include "Engine/World.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.0f;
	MovementSpeed = 0.5f;
	LastMoveDirection = EMovementDirection::DOWN;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(4);
	if (IsValid(FoodClass))
	{
		SpawnFood();
	}
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; ++i)
	{
		FVector NewLocation(SnakeElements.Num()*ElementSize, 0.0f, 0.0f);
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElem->SnakeOwner = this;
		int32 ElemIndex = SnakeElements.Add(NewSnakeElem);
		if (ElemIndex == 0)
		{
			NewSnakeElem->SetFirstElementType();
		}
	}
}

void ASnakeBase::DestroySnakeElements()
{
	for (int i = 0; i < SnakeElements.Num(); ++i)
	{
		SnakeElements[i]->Destroy();
	}
}

void ASnakeBase::Move()
{
	FVector MovementVector(ForceInitToZero);
	
	switch(LastMoveDirection)
	{
		case EMovementDirection::UP:
			MovementVector.X += ElementSize;
			break;
		case EMovementDirection::DOWN:
			MovementVector.X -= ElementSize;
			break;
		case EMovementDirection::LEFT:
			MovementVector.Y += ElementSize;
			break;
		case EMovementDirection::RIGHT:
			MovementVector.Y -= ElementSize;
			break;
	}

	//AddActorWorldOffset(MovementVector);

	SnakeElements[0]->ToggleCollision();

	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}
	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		SnakeElements[i]->ToggleVisible();
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
	SnakeElements[0]->ToggleVisible();
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}

void ASnakeBase::SpawnFood()
{
	FVector FoodLocation(0.0f, 0.0f, 0.0f);
	bool CorrectLocation = 1;
	while (CorrectLocation)
	{
		CorrectLocation = 0.0f;
		FoodLocation = { ElementSize * FMath::RandRange(int32(-8), int32(8)),
						 ElementSize * FMath::RandRange(int32(-8), int32(8)), 0.0f };

		for (int i = 0; i < SnakeElements.Num(); i++)
		{
			FVector ThisElementLocation = SnakeElements[i]->GetActorLocation();
			if (((FoodLocation.X >= ThisElementLocation.X - ElementSize)
				&& (FoodLocation.X <= ThisElementLocation.X + ElementSize))
				&& ((FoodLocation.Y >= ThisElementLocation.Y - ElementSize)
				&& (FoodLocation.Y <= ThisElementLocation.Y + ElementSize)))
			{
				CorrectLocation = 1;
			}
		}
	}
	FTransform NewTransform(FoodLocation);
	GetWorld()->SpawnActor<AFood>(FoodClass, NewTransform);
}
